with borrower as (
	select 
		borrower_id,
		state,
		city,
		zip_code,
		borrower_credit_score
	from Borrower_table
),
loan as (
	select 
		borrower_id,
		loan_id,
		date_of_release::date,
		term,
		interestrate,
		loanamount,
		downpayment,
		payment_frequency,
		case when maturity_date = '02/29/2023' then '2023-03-01'::date else maturity_date::date end as maturity_date
	from Loan_table
),
payments_schedules as (
	select 
		loan_id,
		schedule_id,
		expected_payment_date::date,
		expected_payment_amount
	from Payment_Schedule
),
loan_payment as (
	select 
		loan_id,
		payment_id,
		amount_paid,
		date_paid::date,
		replace(payment_id, 'PAID','') schedule_id
	from Loan_payment order by 1,4 desc
),
borrower_loan_summary_dim as (
select 
	b.borrower_id,
	b.state,
	b.city,
	b.zip_code,
	b.borrower_credit_score,
	l.loan_id,
	ps.schedule_id,
	date_of_release,
	term,
	interestrate,
	loanamount,
	downpayment, 
	payment_frequency,
	maturity_date,
	expected_payment_date,
	expected_payment_amount,
	min(expected_payment_date) over (partition by ps.loan_id) as scheduled_start_expected_payment_date,
 	max(expected_payment_date) over (partition by lp.loan_id) as scheduled_end_expected_payment_date,
	ROW_NUMBER() OVER (PARTITION BY lp.loan_id ORDER BY lp.date_paid DESC) AS rn
from borrower b
left join loan l on l.borrower_id = b.borrower_id
left join payments_schedules ps on ps.loan_id = l.loan_id
left join loan_payment lp on lp.loan_id = l.loan_id
order by 1,7
),
payment_schedule_loan_dim as (
select 
	lp.payment_id,
	ps.loan_id,
	ps.schedule_id,
	ps.expected_payment_date as scheduled_payment_date,
	ps.expected_payment_amount as scheduled_payment_amount,
	date_paid,
	amount_paid,
	abs(expected_payment_date - date_paid) as par_days,
	case when abs(expected_payment_date - date_paid) != 0 then amount_paid else null end as primary_amount_risk,
	(date_part('Month', date_paid) - date_part('Month', expected_payment_date)) AS month_diff,
	lead(expected_payment_date) over (partition by ps.loan_id order by expected_payment_date desc) as lead_expected_payment_date 
from payments_schedules ps 
left join loan_payment lp on lp.loan_id = ps.loan_id and ps.schedule_id = lp.schedule_id
order by 1, 3 desc
), 
payment_loans as (
select 
	payment_id,
	loan_id,
	schedule_id,
	scheduled_payment_date,
	scheduled_payment_amount,
	date_paid,
	amount_paid,
	par_days,
	case when lead_expected_payment_date is null then primary_amount_risk::numeric 
		 when month_diff = 1 then primary_amount_risk::numeric 
		 when month_diff > 1 then primary_amount_risk::numeric * month_diff
		 else null end as amount_risk
from payment_schedule_loan_dim
),
summary as (
select 
	b.*,
	p.payment_id,
	p.schedule_id,
	p.scheduled_payment_date,
	p.scheduled_payment_amount,
	p.date_paid,
	p.amount_paid,
	p.par_days,
	coalesce(p.amount_risk, 0) as amount_risk
from borrower_loan_summary_dim b
left join payment_loans p on p.loan_id = b.loan_id
where rn = 1
order by borrower_id, date_of_release
),
missed_payments as (
select * from summary where par_days != 0
)
select * from missed_payments
