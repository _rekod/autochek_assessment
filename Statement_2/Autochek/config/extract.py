import os
from config.config import Config
from config.log_module import Log



class Extract(Config):
    logger = Log(__name__)
    paths = None

    def __init__(self):
        self.get_path()

    def get_path(self):
        _path = self.get_environment()
        self.paths = _path
