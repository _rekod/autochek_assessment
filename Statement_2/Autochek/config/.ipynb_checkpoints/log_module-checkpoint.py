import logging
from datetime import datetime
import os


class Log:

    def __init__(self, name):
        self.logger = logging.getLogger(name)
        log_file = os.getcwd() + '/config/storage/logs/' + datetime.today().strftime('%Y-%m-%d') + '-currency_autochek.log'
        # create handler

        file_handler = logging.FileHandler(log_file)

        # level and format
        file_handler.setLevel(logging.DEBUG)
        file_handler.setLevel(logging.INFO)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)

        self.logger.addHandler(file_handler)

