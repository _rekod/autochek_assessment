from configparser import ConfigParser
import os
from config.log_module import Log


class Config:
    logger = Log(__name__)
    filename = os.getcwd() + '/config/database.ini'
    environment = os.getcwd() + '/config/environment.ini'

    def __init__(self)->None:
        pass

    # GET Automation Environment paths from storage folder and load them as an object.
    def get_environment(self, filename=environment, section='application'):
        parser = ConfigParser()
        parser.read(filename)
        env = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                env[param[0]] = param[1]
        else:
            self.logger.logger.exception('Environment {0} not found '.format(filename))

        return env

    # GET Database connection properties from storage folder and load them as an object.
    def get_configuration(self, filename=filename, section='postgresql'):
        parser = ConfigParser()
        parser.read(filename)

        db = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db[param[0]] = param[1]
        else:
            self.logger.logger.exception('Environment {0} not found '.format(filename))

        return db

    