import pandas as pd
from config.setup.database import Database
from config.log_module import Log
from config.setup.staging import Staging
from config.database.ddl.sql_builder import SQLBuilder
import json

class ResultsSetup:
    logger = Log(__name__)
    database = Database()
    database_ddl = SQLBuilder()
    df = pd.DataFrame()
    data = None

    def __init__(self, results):
        self.save_data(results)
    
    def save_data(self, results):
        try:
            self.database.execute_values(results, "data")
            self.logger.logger.error("Data insert successfully")
        except Exception as error:
            self.logger.logger.exception(error)
    