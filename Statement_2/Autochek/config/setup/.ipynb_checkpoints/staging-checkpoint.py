import csv
import os
from config.log_module import Log

class Staging:
    logger = Log(__name__)

    path = os.getcwd()+'/config/storage/staging/'

    def __init__(self, df, columns, name):
        self.write_temp(df, columns, name)


    def write_temp(self, df, columns, name):
        try:
            with open(self.path+name+'.csv', 'w') as file:
             w = csv.writer(file)
             w.writerow(columns)
             w.writerows(df.values)
        except Exception as error:
            self.logger.logger.exception(error)