import pandas as pd
from config.setup.database import Database
from config.log_module import Log
from config.setup.staging import Staging
from config.database.ddl.sql_builder import SQLBuilder
import json

class CurrencySetup:
    logger = Log(__name__)
    database = Database()
    database_ddl = SQLBuilder()
    df = pd.DataFrame()
    currencies = list()
    path = None

    def __init__(self, path):
        self.path = path
        self.get_currencies()
    
    def get_currencies(self):
        self.df = self.database_ddl.get_currencies()
        return self.df
    
#     def extract(self, path):
#         try:
#             with open(path) as entries:
#                 for entry in entries:
#                     self.list_of_entries.append(entry)
#             self.build_candidates_data()
#         except Exception as error:
#             self.logger.logger.exception(error)


#     def build_candidate_data(self, entry):
#         candidate = Candidate()
#         return candidate


#     def convert_list_to_dataframe(self):
#         self.df = pd.DataFrame([candidate.__dict__ for candidate in self.candidates])
    