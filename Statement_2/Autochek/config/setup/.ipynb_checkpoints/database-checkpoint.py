import os
import pandas as pd
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import psycopg2.extras as extras
from config.config import Config
from config.log_module import Log


class Database:
    config = Config()
    logger = Log(__name__)
    conn = None
    engine = None

    def __init__(self):
        self.dump_sql()

    # RUN SQL DUMPS TO SET UP DATABASE SCHEMAS AND RESOURCES.
    def dump_sql(self):
        self.logger.logger.error("dumping schema in database...")
        test_connection = self.run_query("select table_name from information_schema.tables where table_schema = 'public' \
        and table_name = 'data' limit 1;")
        self.logger.logger.error(' SCHEMA: '+ str(test_connection.shape[0]) )
        if test_connection.shape[0] == 0:
            try:
                self.connect()
                schema = os.getcwd() + '/config/database/schema.sql'
                default = os.getcwd() + '/config/database/default.sql'
                with self.conn.cursor() as cursor:
                    cursor.execute(open(schema, "r").read())
                    cursor.execute(open(default, "r").read())
                self.conn.commit()
            except Exception as error:
                self.logger.logger.exception(error)
            finally:
                cursor.close()

    # ONLY CONNECTION TO DATABASE.
    def connect(self):
        try:
            params = self.config.get_configuration()
            self.conn = psycopg2.connect(**params)
            self.logger.logger.info('database connection open')
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.logger.exception(error)
        return self.conn

    # RUN QUERIES PASSED IN AS STRINGS AND RETURN DATAFRAME AS RESPONSE, SHOULD BE USED MOSTLY WHEN
    # REQUIRED TO RETURN MORE THAN ONE ELEMENT.
    def run_query(self, query):
        self.connect()
        try:
            df = pd.read_sql_query(query, self.conn)
            return df
        except Exception as error:
            self.logger.logger.exception(error)
        finally:
            self.close_connection()

    # RUN QUERIES PASSED IN AS STRINGS AND RETURN DATAFRAME AS RESPONSE, SHOULD BE USED MOSTLY WHEN
    # REQUIRED TO RETURN MORE THAN ONE ELEMENT.
    def run_dml_query(self, query):
        self.connect()
        try:
            cur = self.conn.cursor()
            cur.execute(query)
            self.conn.commit()
        except Exception as error:
            self.logger.logger.exception(error)
            self.conn.rollback()
        finally:
            self.close_connection()

    def execute_values(self, df, table):
        self.connect()
        self.logger.logger.error(table)
        # Create list of tuples from the dataframe values
        tuples = [tuple(x) for x in df.to_numpy()]

        cols = ','.join(list(df.columns))
        query = "INSERT INTO %s(%s) VALUES %%s" % (table, cols)
        cursor = self.conn.cursor()
        try:
            extras.execute_values(cursor, query, tuples)
            self.conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.logger.exception(error, exc_info=True )
            self.conn.rollback()
            return 1
        finally:
            cursor.close()
            self.close_connection()

    # CLOSE DATABASE CONNECTION, TRY TO ADD IT WHEN ALL APPLICATION COMMUNICATION TO THE DATABASE IS DONE.
    def close_connection(self):
        if self.conn is not None:
            self.conn.close()
            self.logger.logger.info("connection to db is closed")
    