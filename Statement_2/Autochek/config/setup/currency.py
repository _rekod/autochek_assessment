import pandas as pd
from config.setup.database import Database
from config.log_module import Log
from config.setup.staging import Staging
from config.database.ddl.sql_builder import SQLBuilder
import json

class CurrencySetup:
    logger = Log(__name__)
    database = Database()
    database_ddl = SQLBuilder()
    df = pd.DataFrame()
    currencies = list()
    path = None

    def __init__(self, path):
        self.path = path
        self.get_currencies()
    
    def get_currencies(self):
        self.df = self.database_ddl.get_currencies()
        return self.df
    