
CREATE SEQUENCE currencies_id_seq;
create table IF NOT EXISTS currencies (
	id bigint NOT NULL DEFAULT nextval('currencies_id_seq'),
	name varchar(50),
	code varchar(3),
	description text,
	CONSTRAINT pk_currency_id PRIMARY KEY (id)
);


CREATE SEQUENCE countries_id_seq;
create table IF NOT EXISTS countries (
	id bigint NOT NULL DEFAULT nextval('countries_id_seq'),
	name varchar(50),
	description text,
	currency_id integer,
	created_at timestamp DEFAULT CURRENT_TIMESTAMP,
	update_at timestamp DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT fk_currency_id
        FOREIGN KEY(currency_id)
            REFERENCES currencies(id)	
);

CREATE SEQUENCE data_id_seq;
create table IF NOT EXISTS data (
	id bigint NOT NULL DEFAULT nextval('data_id_seq'),
	timestamp timestamp,
	currency_from text,
	USD_to_currency_rate text,
	currency_to_USD_rate text,
	currency_to text,
	created_at timestamp DEFAULT CURRENT_TIMESTAMP,
	updated_at timestamp DEFAULT CURRENT_TIMESTAMP
);


CREATE OR REPLACE VIEW countries_dim
 AS
 select 
 	c.name as country,
	cc.name as currency,
	cc.code as code_currency
 from countries c 
 left join currencies cc on cc.id = c.currency_id;

