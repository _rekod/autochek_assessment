INSERT INTO currencies (code, name, description) VALUES
    ('NGN', N'Nigerian Naira', N'Nigerian Naira'),
    ('GHS', N'Ghanaian Cedi', N'Ghanaian Cedi'),    
    ('KES', N'Uganda Shilling', N'Uganda Shilling'),
    ('UGX', N'Kenyan Shilling', N'Kenyan Shilling'),
    ('MAD', N'Moroccan Dirham', N'Moroccan Dirham'),
    ('XOF', N'West African CFA franc', N'West African CFA franc'),
    ('EGP', N'Egyptian Pound', N'Egyptian Pound');
	
INSERT INTO countries (name, description, currency_id) VALUES
    (N'Nigeria', N'Nigeria', 1),
    (N'Ghana', N'Ghana', 2),    
    (N'Uganda', N'Uganda', 3),
    (N'Kenya', N'Kenya', 3),
    (N'Morocco', N'Morocco', 4),
    (N'Côte dIvoire', N'Côte dIvoire', 4),
    (N'Egypt', N'Egypt', 6);
	