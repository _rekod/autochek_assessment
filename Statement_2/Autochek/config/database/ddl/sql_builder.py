from config.setup.database import Database

class SQLBuilder:

    database = Database()

    def __init__(self):
        pass

    def get_currencies(self):
        return self.database.run_query("select * from countries_dim")
