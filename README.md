# AutoChek Assessment

## Problem Statement 1 

Statement_1 directory contains Solution completed with SQL, 
Solution consists of using CTE's to map transform where needed and map out the business logic.
Attached also in the repo is the output xls file

## Problem Statement 2

Statement_2 directory contains Solution completed using JupyterLab;
The project is run by running the CurrencyRate notebook which then creates the required database resources.

The solution uses the environment and database configurations to get the database connection attributes as well as the 
application attributes.

